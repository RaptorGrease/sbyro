extends Spatial


# constants
const UP : Vector3 = Vector3.UP
const GRAVITY : Vector3 = Vector3.DOWN * 10.0
const LIFT : Vector3 = Vector3.UP * 9.9
const SPEED : float = 20.0
const CHARGESPEED : float = 40.0
const JUMPHEIGHT : float = 2.0
var MULTIPLIER : float = 32.0

# referenced nodes
var model : KinematicBody
var collider_body : CollisionShape
var collider_head : CollisionShape
var collider_left_foreleg : CollisionShape
var collider_left_hindleg : CollisionShape
var collider_right_foreleg : CollisionShape
var collider_right_hindleg : CollisionShape
var snout : MeshInstance
var target : Spatial
var rotation_helper : Spatial
var inner_helper : Spatial
var camera : Camera
var label : Label
var label2 : Label
var line_edit : LineEdit
var hover_timer : Timer
var flame_particle : PackedScene
var flame_splash_particle : PackedScene
var flame_ray : RayCast
var flame_ray2 : RayCast
var flame_ray3 : RayCast
var point : MeshInstance
var body_root : MeshInstance
var neck_root : Spatial
var head : MeshInstance
var left_wing_root : Spatial
var right_wing_root : Spatial
var tail_root : Spatial

# variables
var target_rotation : float = 0.0
var t : float = 0.0
var target_point : Vector3
var velocity : Vector3 = Vector3()
var prev_velocity : Vector3 = Vector3()
var flame : Particles
var glide : bool = false
var jump : bool = false
var glide_cancel : bool = false
var wall_bump : bool = false
var hover : bool = false
var flammable : Array


func _ready() -> void:
	model = $Model
	collider_body = $Model/ColliderBody
	collider_head = $Model/ColliderHead
	collider_left_foreleg = $Model/ColliderLeftForeleg
	collider_left_hindleg = $Model/ColliderLeftHindleg
	collider_right_foreleg = $Model/ColliderRightForeleg
	collider_right_hindleg = $Model/ColliderRightHindleg
	body_root = $Model/Body
	neck_root = $Model/Body/NeckRoot
	head = $Model/Body/NeckRoot/Neck/Head
	snout = $Model/Body/NeckRoot/Neck/Head/Snout
	left_wing_root = $Model/Body/LeftWingRoot
	right_wing_root = $Model/Body/RightWingRoot
	tail_root = $Model/Body/TailRoot
	rotation_helper = $RotationHelper
	inner_helper = $RotationHelper/InnerHelper
	camera = $RotationHelper/InnerHelper/Camera
	label = $Label
	label2 = $Label2
	line_edit = $LineEdit
	hover_timer = $HoverTimer
	target = $Target
	point = $Point
	flame_ray = $Model/Body/NeckRoot/Neck/Head/Snout/FlameRay
	flame_ray2 = $Model/Body/NeckRoot/Neck/Head/Snout/FlameRay2
	flame_ray3 = $Model/Body/NeckRoot/Neck/Head/Snout/FlameRay3
	flame_particle = preload("res://Particle_Flame.tscn")
	flame_splash_particle = preload("res://Particle_Flame_Splash.tscn")
	
	inner_helper.rotation_degrees.x = -30.0
	target_point = -camera.global_transform.basis.x.cross(UP)
	line_edit.text = str(MULTIPLIER)
	Engine.target_fps = 60
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _process(delta : float) -> void:
# toggle mouse capture when cancel button is pressed
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
# getting target rotation
	if target_point.x == 0 and target_point.z == 0:
		target_point = -model.transform.basis.z
	target.look_at(model.transform.origin + target_point, UP)
# debug using Point MeshInstance
#	point.translation = model.transform.origin + target_point * 3
# figure out wanted rotation
	var diff = fmod(target.rotation_degrees.y - model.rotation_degrees.y + 180.0, 360.0) - 180.0
	diff = diff + 360.0 if diff < -180.0 else diff
	target_rotation = model.rotation_degrees.y + diff
# actual rotation
	model.rotation_degrees.y = lerp(model.rotation_degrees.y, target_rotation, 0.25)
# label stuff
	label2.text = str(rotation_helper.rotation_degrees)


func _physics_process(delta) -> void:
	var cam_xform = camera.global_transform
	var dir : Vector3 = Vector3()
	var glide_dir : Vector3 = Vector3()
	var brake : bool = false
	var charge : bool = false
	
	prev_velocity = velocity
# take basic input
	if Input.is_action_pressed("ui_up"):
		dir -= cam_xform.basis.x.cross(UP)
		target_point -= cam_xform.basis.x.cross(UP) * 2
	if Input.is_action_pressed("ui_right"):
		var xproduct = UP.cross(cam_xform.basis.z)
		if xproduct.x > -0.01 and xproduct.x < 0.01:
			dir += Vector3.RIGHT
			target_point += Vector3.RIGHT * 2
		else:
			dir += xproduct
			target_point += xproduct * 2
		glide_dir = model.transform.basis.x * 0.1
	if Input.is_action_pressed("ui_down"):
		dir += cam_xform.basis.x.cross(UP)
		target_point += cam_xform.basis.x.cross(UP) * 2
		if glide:
			brake = true
	if Input.is_action_pressed("ui_left"):
		var xproduct = UP.cross(cam_xform.basis.z)
		if xproduct.x > -0.01 and xproduct.x < 0.01:
			dir += Vector3.LEFT
			target_point += Vector3.LEFT * 2
		else:
			dir -= xproduct
			target_point -= xproduct * 2
		glide_dir = -model.transform.basis.x * 0.1
# initiate charge when shift is pressed and not bumping into a wall
	if Input.is_action_pressed("kb_shift") and !wall_bump:
		charge = true
# spit fire on left click
	if Input.is_action_just_pressed("fire1") and !is_instance_valid(flame) and !charge and !wall_bump:
		var flame_magnitude : float = 1.0
		if flame_ray.is_colliding():
			var collision_point : Vector3 = flame_ray.get_collision_point()
			flame_magnitude = collision_point.distance_to(snout.global_transform.origin) / 5.0
			var splash : Particles = flame_splash_particle.instance() as Particles
			get_tree().get_root().add_child(splash)
			splash.global_transform.origin = collision_point
			splash.look_at(collision_point + flame_ray.get_collision_normal(), UP)
			splash.rotation_degrees.x = 90.0
		if flame_ray2.is_colliding():
			var collision_point : Vector3 = flame_ray2.get_collision_point()
			var splash : Particles = flame_splash_particle.instance() as Particles
			get_tree().get_root().add_child(splash)
			splash.global_transform.origin = collision_point
			splash.look_at(collision_point + flame_ray2.get_collision_normal(), UP)
			splash.rotation_degrees.x = 90.0
		if flame_ray3.is_colliding():
			var collision_point : Vector3 = flame_ray3.get_collision_point()
			var splash : Particles = flame_splash_particle.instance() as Particles
			get_tree().get_root().add_child(splash)
			splash.global_transform.origin = collision_point
			splash.look_at(collision_point + flame_ray3.get_collision_normal(), UP)
			splash.rotation_degrees.x = 90.0
		flame = flame_particle.instance() as Particles
		flame.process_material.set("initial_velocity", -8.0 * flame_magnitude)
		snout.add_child(flame)
		flame.translation = Vector3(0.0, -0.35, -0.35)
		for f in flammable:
			f.burn()
	if Input.is_action_just_pressed("kb_f") and glide:
		hover = true
		glide = false
		hover_timer.start()
		velocity.y = JUMPHEIGHT * MULTIPLIER
# normalize input direction and target_point
	dir = dir.normalized()
	target_point = target_point.normalized()
# apply target_point and input
	if glide:
		velocity.z = 0.0
		velocity.x = 0.0
		target_point = -model.transform.basis.z + glide_dir
	elif charge:
		velocity.x = 0.0
		velocity.z = 0.0
		target_point = -model.transform.basis.z + glide_dir
	elif !wall_bump:
		velocity.x = dir.x * SPEED * delta * MULTIPLIER
		velocity.z = dir.z * SPEED * delta * MULTIPLIER
# apply jump
	if Input.is_action_just_pressed("ui_select"):
		if model.is_on_floor():
			jump = true
			velocity.y = JUMPHEIGHT * MULTIPLIER
		elif !glide and !wall_bump and !hover:
			glide = true
			glide_cancel = false
			jump = false
			velocity.y = GRAVITY.y * delta * MULTIPLIER
			t = 0.0
	if Input.is_action_just_released("ui_select"):
		if velocity.y > 32.0:
			velocity.y = 32.0
# apply glide
	if glide:
		if charge:
			glide = false
			glide_cancel = true
			velocity.y = GRAVITY.y * delta * MULTIPLIER
		else:
			velocity += LIFT * delta * MULTIPLIER
			velocity -= model.transform.basis.z * SPEED * delta * MULTIPLIER
		if brake:
			velocity.z *= 0.5
			velocity.x *= 0.5
# apply charge
	if charge:
		velocity -= model.transform.basis.z * CHARGESPEED * delta * MULTIPLIER
# apply gravity
	velocity += GRAVITY * delta * MULTIPLIER
	if velocity.y < -(JUMPHEIGHT * MULTIPLIER):
		velocity.y = -(JUMPHEIGHT * MULTIPLIER)
# apply movement
	velocity = model.move_and_slide(velocity, UP, true)
# check for a jump and glide related physics issue
	if velocity.y > prev_velocity.y and !jump and !glide_cancel and !hover:
		velocity.y = prev_velocity.y
	if model.get_slide_count() > 0 and !model.is_on_wall():
		var hit : KinematicCollision = model.get_slide_collision(0)
		model.transform.basis = model.transform.basis.slerp(Basis(hit.normal.cross(model.transform.basis.z), hit.normal, model.transform.basis.x.cross(hit.normal)).orthonormalized(), 0.25)
	else:
		model.transform.basis = model.transform.basis.slerp(Basis(UP.cross(model.transform.basis.z), UP, model.transform.basis.x.cross(UP)).orthonormalized(), 0.25)
# reset values if model is on floor
	if model.is_on_floor():
		jump = false
		glide = false
		wall_bump = false
		hover = false
		velocity.y = 0.0
# check for glide or charge
	if glide or charge:
# rotate camera behind player
		var diff = fmod(model.rotation_degrees.y - rotation_helper.rotation_degrees.y + 180.0, 360.0) - 180.0
		diff = diff + 360.0 if diff < -180.0 else diff
		var cam_rotation = rotation_helper.rotation_degrees.y + diff
		rotation_helper.rotation_degrees.y = lerp(rotation_helper.rotation_degrees.y, cam_rotation, 0.1)
# check for collision with walls
		if model.is_on_wall():
			glide = false
			charge = false
			wall_bump = true
			velocity = (model.transform.basis.z * SPEED) + (UP * SPEED * JUMPHEIGHT)
# move related nodes to current model position
	rotation_helper.translation = model.translation
	target.translation = model.translation
#	if velocity.y < 0.0 and prev_velocity.y >= 0.0:
#		jump_apex = true
# animation stuff
	if glide or hover:
		left_wing_root.rotation_degrees.y = lerp(left_wing_root.rotation_degrees.y, 0.0, 0.25)
		right_wing_root.rotation_degrees.y = lerp(right_wing_root.rotation_degrees.y, 180.0, 0.25)
		tail_root.rotation_degrees.x = lerp(tail_root.rotation_degrees.x, -10.0, 0.25)
		if brake or hover:
			body_root.rotation_degrees.x = lerp(body_root.rotation_degrees.x, 45.0, 0.25)
			neck_root.rotation_degrees.x = lerp(neck_root.rotation_degrees.x, -50, 0.25)
			if hover:
				left_wing_root.rotation_degrees.z = -rad2deg(sin(-2 + t * 10)) * 0.75
				right_wing_root.rotation_degrees.z = -rad2deg(sin(-2 + t * 10)) * 0.75
			else:
				left_wing_root.rotation_degrees.z = -rad2deg(sin(t * 25)) * 0.25
				right_wing_root.rotation_degrees.z = -rad2deg(sin(t * 25)) * 0.25
			collider_body.rotation_degrees.x = lerp(collider_body.rotation_degrees.x, 45.0, 0.25)
			collider_left_foreleg.translation = collider_left_foreleg.translation.linear_interpolate(Vector3(-0.5, -0.46, -1.52), 0.25)
			collider_left_foreleg.rotation_degrees.x = -45.0
			collider_left_hindleg.translation = collider_left_hindleg.translation.linear_interpolate(Vector3(-0.5, -1.52, -0.46), 0.25)
			collider_left_hindleg.rotation_degrees.x = -45.0
			collider_right_foreleg.translation = collider_right_foreleg.translation.linear_interpolate(Vector3(0.5, -0.46, -1.52), 0.25)
			collider_right_foreleg.rotation_degrees.x = -45.0
			collider_right_hindleg.translation = collider_right_hindleg.translation.linear_interpolate(Vector3(0.5, -1.52, -0.46), 0.25)
			collider_right_hindleg.rotation_degrees.x = -45.0
		else:
			left_wing_root.rotation_degrees.z = -rad2deg(sin(t)) * 0.2
			right_wing_root.rotation_degrees.z = -rad2deg(sin(t)) * 0.2
		t += delta
	else:
		left_wing_root.rotation_degrees.y = lerp(left_wing_root.rotation_degrees.y, 45.0, 0.25)
		right_wing_root.rotation_degrees.y = lerp(right_wing_root.rotation_degrees.y, 135.0, 0.25)
		if velocity.y < -0.1:
			left_wing_root.rotation_degrees.z = lerp(left_wing_root.rotation_degrees.z, -45.0, 0.25)
			right_wing_root.rotation_degrees.z = lerp(right_wing_root.rotation_degrees.z, -45.0, 0.25)
			tail_root.rotation_degrees.x = lerp(tail_root.rotation_degrees.x, -45.0, 0.25)
		elif velocity.y > 0.1:
			left_wing_root.rotation_degrees.z = lerp(left_wing_root.rotation_degrees.z, 30.0, 0.25)
			right_wing_root.rotation_degrees.z = lerp(right_wing_root.rotation_degrees.z, 30.0, 0.25)
			tail_root.rotation_degrees.x = lerp(tail_root.rotation_degrees.x, 30.0, 0.25)
		else:
			left_wing_root.rotation_degrees.z = 0.0
			right_wing_root.rotation_degrees.z = 0.0
			tail_root.rotation_degrees.x = 10.0
	
	if !brake and !hover:
		body_root.rotation_degrees.x = lerp(body_root.rotation_degrees.x, 0.0, 0.25)
		collider_body.rotation_degrees.x = lerp(collider_body.rotation_degrees.x, 0.0, 0.25)
		collider_left_foreleg.translation = collider_left_foreleg.translation.linear_interpolate(Vector3(-0.5, -1.4, -0.75), 0.25)
		collider_left_foreleg.rotation_degrees.x = 90.0
		collider_left_hindleg.translation = collider_left_hindleg.translation.linear_interpolate(Vector3(-0.5, -1.4, 0.75), 0.25)
		collider_left_hindleg.rotation_degrees.x = 90.0
		collider_right_foreleg.translation = collider_right_foreleg.translation.linear_interpolate(Vector3(0.5, -1.4, -0.75), 0.25)
		collider_right_foreleg.rotation_degrees.x = 90.0
		collider_right_hindleg.translation = collider_right_hindleg.translation.linear_interpolate(Vector3(0.5, -1.4, 0.75), 0.25)
		collider_right_hindleg.rotation_degrees.x = 90.0
	
	if charge:
		neck_root.rotation_degrees.x = lerp(neck_root.rotation_degrees.x, -50.0, 0.25)
		collider_head.disabled = false
		collider_head.translation = collider_head.translation.linear_interpolate(Vector3(0.0, 1.2, -1.825), 0.25)
	else:
		collider_head.disabled = true
	
	if is_instance_valid(flame) and (!brake and !hover and !wall_bump):
		neck_root.rotation_degrees.x = lerp(neck_root.rotation_degrees.x, -50, 0.25)
		head.rotation_degrees.x = lerp(head.rotation_degrees.x, -40, 0.25)
		collider_head.translation = collider_head.translation.linear_interpolate(Vector3(0.0, 1.2, -1.825), 0.25)
	elif !brake and !charge and !hover:
		neck_root.rotation_degrees.x = lerp(neck_root.rotation_degrees.x, 0, 0.25)
		head.rotation_degrees.x = lerp(head.rotation_degrees.x, -90, 0.25)
		collider_head.translation = collider_head.translation.linear_interpolate(Vector3(0.0, 1.75, -1.15), 0.25)
# label stuff
	#label.text = str("on floor: ",model.is_on_floor(),", on wall: ", model.is_on_wall(), " gliding: ", glide)
	label.text = str(velocity)


func _input(event) -> void:
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		inner_helper.rotate_x(deg2rad(event.relative.y * 0.05 * -1))
		rotation_helper.rotate_y(deg2rad(event.relative.x * 0.05 * -1))
		inner_helper.rotation_degrees.x = clamp(inner_helper.rotation_degrees.x, -90, 90)


func _on_FlameArea_body_entered(body : Node) -> void:
	for child in body.owner.get_children():
		if child.has_method("burn"):
			flammable.append(child)
			if is_instance_valid(flame):
				child.burn()


func _on_FlameArea_body_exited(body : Node) -> void:
	for child in body.owner.get_children():
		if child.has_method("burn"):
			flammable.erase(child)


func _on_HoverTimer_timeout() -> void:
	hover = false


func _on_LineEdit_text_entered(new_text) -> void:
	MULTIPLIER = abs(float(new_text))
