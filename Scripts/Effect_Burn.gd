extends Node

# Declare member variables here
var mesh : MeshInstance
var original_material : SpatialMaterial
export var burn_material : SpatialMaterial = null
var burn_timer : Timer


# Called when the node enters the scene tree for the first time
func _ready() -> void:
	for child in get_owner().get_children():
		if child is MeshInstance:
			mesh = child
	original_material = mesh.material_override
	if burn_material == null:
		burn_material = SpatialMaterial.new()
		burn_material.albedo_color = Color.red
	burn_timer = get_node("BurnTimer")


func burn() -> void:
	mesh.material_override = burn_material
	burn_timer.start()


func _on_BurnTimer_timeout() -> void:
	mesh.material_override = original_material
